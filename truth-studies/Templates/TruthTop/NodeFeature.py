def eta(a):
    return float(a.eta)

def energy(a):
    return float(a.e)

def pT(a):
    return float(a.pt)

def phi(a):
    return float(a.phi)

def mass(a):
    return float(a.CalculateMass())

# ==== Truth Feature ===== #
def FromRes(a):
    return float(a.FromRes)


